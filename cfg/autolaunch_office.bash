#!/bin/bash
# file: autolaunch_cart.bash
#
echo "running usb_memory.bash"
sudo /home/kelzal/catkin_ws/src/kelzal_produce_server/cfg/usb_memory.bash

echo "running roscore_cart daemon ..." 
screen -dmS roscore_cart /home/kelzal/catkin_ws/src/kelzal_produce_server/cfg/roscore_cart.bash
sleep 2

echo "running webserver_cart daemon ..."
screen -dmS webserver_cart /home/kelzal/catkin_ws/src/kelzal_produce_server/cfg/webserver_cart.bash

echo "running kelzal_cart daemon ..." 
screen -dmS kelzal_cart bash -c " source /opt/ros/melodic/setup.bash; source /home/kelzal/catkin_ws/devel/setup.bash; source /home/kelzal/catkin_ws/src/kelzal_produce_server/cfg/master.sh; roslaunch kelzal_produce_server produce_all.launch fruit_debug:=False office_debug:=True use_sr:=False config_file:=/home/kelzal/catkin_ws/src/spinnaker_sdk_camera_driver/params/office_params.yaml; bash"
echo "done. view with screen -ls" 
echo "To see a session: screen -r name 'roscore_cart' or 'kelzal_cart' or others"
echo "To kill that screen session and keep running: Ctrl-A-D instead of Ctrl-C"  
