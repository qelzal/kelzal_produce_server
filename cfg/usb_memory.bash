#!/bin/bash
# file: usb_memory.bash
# https://askubuntu.com/questions/425754/how-do-i-run-a-sudo-command-inside-a-script
# run sudo visudo and add at the end of the file before the line #includedir ... 
# username ALL=(ALL) NOPASSWD: /path/to/script
# demo ALL= NOPASSWD: /home/kelzal/catkin_ws/src/kelzal_produce_server/scripts/usb_memory.bash
# then still call that file with sudo: 
# sudo /home/demo/ros/catkin_ws3/src/spinnaker_sdk_camera_driver/scripts/usbflir.bash
echo "increasing USB buffer size for cameras to 1000 Mb instead of default 16 Mb"
#sh -c 'echo 1000 > /sys/module/usbcore/parameters/usbfs_memory_mb'
echo 1000 > /sys/module/usbcore/parameters/usbfs_memory_mb
echo "jetson_clocks Xavier Maximizing Performance"
jetson_clocks
