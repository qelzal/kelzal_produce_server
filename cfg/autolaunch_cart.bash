#!/bin/bash
# file: autolaunch_cart.bash
#
echo "running usb_memory.bash"
sudo /home/kelzal/catkin_ws/src/kelzal_produce_server/cfg/usb_memory.bash

sleep 120

echo "running roscore_cart daemon ..." 
screen -dmS roscore_cart /home/kelzal/catkin_ws/src/kelzal_produce_server/cfg/roscore_cart.bash
sleep 2

echo "running webserver_cart daemon ..."
screen -dmS webserver_cart /home/kelzal/catkin_ws/src/kelzal_produce_server/cfg/webserver_cart.bash

sleep 2

echo "running kelzal_cart daemon ..." 
screen -dmS kelzal_cart /home/kelzal/catkin_ws/src/kelzal_produce_server/cfg/kelzal_cart.bash
echo "done. view with screen -ls" 
echo "To see a session: screen -r name 'roscore_cart' or 'kelzal_cart' or others"
echo "To kill that screen session and keep running: Ctrl-A-D instead of Ctrl-C"  
