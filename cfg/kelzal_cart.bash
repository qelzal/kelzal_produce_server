#!/bin/bash
# file: kelzal_cart.bash
source /opt/ros/melodic/setup.bash
source /home/kelzal/catkin_ws/devel/setup.bash
source /home/kelzal/catkin_ws/src/kelzal_produce_server/cfg/master.sh
roslaunch kelzal_produce_server produce_all.launch fruit_debug:=False use_sr:=False
