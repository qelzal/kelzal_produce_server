#!/usr/bin/env sh                                                               
if [ -z "$1" ]
    then
        echo "No Arguments, default IP"
        ROS_MASTER_URI="http://10.11.12.191:11311"
        ip=$(/sbin/ip -o -4 addr list eth0 | awk '{print $4}' | cut -d/ -f1)
        ROS_IP=$ip
        echo "ROS_IP = $ROS_IP"                                                         
        echo "ROS_MASTER_URI = $ROS_MASTER_URI"
        export ROS_MASTER_URI
        export ROS_IP
else
       # ip=$(/sbin/ip -o -4 addr list eth0 | awk '{print $4}' | cut -d/ -f1)
        ROS_MASTER_URI="http://$1"                                                     
        echo "ROS_MASTER_URI = $ROS_MASTER_URI"                                         
        read -p "Enter Current PC IP: " currentip
        ip=$currentip
        ROS_IP=$ip                                                         
        echo "ROS_IP = $ROS_IP"                                                         
        echo "ROS_MASTER_URI = $ROS_MASTER_URI"
        export ROS_MASTER_URI                                                           
        export ROS_IP   
fi

