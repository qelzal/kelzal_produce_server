#ifndef CAPTURE_HEADER
#define CAPTURE_HEADER

#include "std_include.h"
#include "serialization.h"
#include "camera.h"

#include <boost/archive/binary_oarchive.hpp>
#include <boost/filesystem.hpp>
#include <string>

//ROS
#include "std_msgs/Float64.h"
#include "std_msgs/String.h"
#include "sensor_msgs/Image.h"
//Dynamic reconfigure
#include <dynamic_reconfigure/server.h>
#include <spinnaker_sdk_camera_driver/spinnaker_camConfig.h>

#include "spinnaker_sdk_camera_driver/SpinnakerImageNames.h"
#include "spinnaker_sdk_camera_driver/GetCameraNames.h"

#include <sstream>
#include <image_transport/image_transport.h>

using namespace Spinnaker;
using namespace Spinnaker::GenApi;
using namespace Spinnaker::GenICam;
using namespace cv;
using namespace std;

namespace acquisition {
    
    class Capture {

    public:
    
        ~Capture();
        Capture();
        Capture( ros::NodeHandle node,ros::NodeHandle private_nh);
        Capture( ros::NodeHandle node,ros::NodeHandle private_nh, bool all);

        // remove the copy constructor because mutexes cannot be copied
        Capture(const Capture&) = delete;

        void load_cameras();
        void load_all_cameras();
        void init_all_cameras();
        void set_fast_config(acquisition::Camera& cam);
        
        void init_array();
        void init_all();
        void init_cameras(bool);
        void start_acquisition();
        void end_acquisition();
        void deinit_cameras();
        void acquire_mat_images(int);
        void run();
        void run_all();
        void run_soft_trig();
        void run_mt();
        void run_mt_pub();
        void publish_to_ros(int, char**, float);

        void read_parameters();
        void read_fast_params();
        vector<string> read_camera_params(ros::NodeHandle&);
        void read_camera_params();
        std::string todays_date();
        
        void acquire_images_to_queue(vector<queue<ImagePtr>>*);
        void publish_from_queue(queue<ImagePtr>& img_q, int i);

        bool get_camera_names(spinnaker_sdk_camera_driver::GetCameraNames::Request &req,
                            spinnaker_sdk_camera_driver::GetCameraNames::Response &res);

    private:

        void check_mem();
        void set_defaults();
        void _constructor();
        void init_ros_hooks();
        void set_frame_rate(CameraPtr, float);
        void create_cam_directories();
        void save_mat_frames(int);
        void dynamicReconfigureCallback(spinnaker_sdk_camera_driver::spinnaker_camConfig &config, uint32_t level);

        float mem_usage();
    
        SystemPtr system_;    
        CameraList camList_;


        vector<acquisition::Camera> cams;
        vector<string> cam_ids_;
        vector<string> cam_names_;
        string master_cam_id_;
        unsigned int numCameras_;
        vector<CameraPtr> pCams_;
        vector<ImagePtr> pResultImages_;
        vector<Mat> frames_;
        vector<string> time_stamps_;
        vector< vector<Mat> > mem_frames_;
        vector<vector<double>> intrinsic_coeff_vec_;
        vector<vector<double>> distortion_coeff_vec_;
        vector<vector<double>> rect_coeff_vec_;
        vector<vector<double>> proj_coeff_vec_;
        vector<string> imageNames;
           
        string path_;
        string todays_date_;

        time_t time_now_;
        double grab_time_, save_time_, toMat_time_, save_mat_time_, export_to_ROS_time_, achieved_time_;

        int nframes_;
        float init_delay_;
        int skip_num_;
        float master_fps_;
        int binning_;
        bool color_;
        string dump_img_;
        string ext_;
        float exposure_time_;
        double target_grey_value_;
        // int decimation_;

        int soft_framerate_; // Software (ROS) frame rate
        
        int MASTER_CAM_;
        int CAM_; // active cam during live

        bool FIXED_NUM_FRAMES_;
        bool TIME_BENCHMARK_;
        bool MASTER_TIMESTAMP_FOR_ALL_;
        bool SAVE_;
        bool SAVE_BIN_;
        bool MANUAL_TRIGGER_;
        bool LIVE_;
        bool CAM_DIRS_CREATED_;
        bool GRID_VIEW_;
//        bool MEM_SAVE_;
        bool SOFT_FRAME_RATE_CTRL_;
        bool EXPORT_TO_ROS_;
        bool MAX_RATE_SAVE_;
        bool PUBLISH_CAM_INFO_;

        // grid view related variables
        bool GRID_CREATED_;
        Mat grid_;

        // ros variables
        ros::NodeHandle nh_;
        ros::NodeHandle nh_pvt_;
        //image_transport::ImageTransport it_;
        image_transport::ImageTransport it_;
        dynamic_reconfigure::Server<spinnaker_sdk_camera_driver::spinnaker_camConfig>* dynamicReCfgServer_;

        ros::Publisher acquisition_pub;
        ros::ServiceServer serial_number_server;
        //vector<ros::Publisher> camera_image_pubs;
        vector<image_transport::CameraPublisher> camera_image_pubs;
        //vector<ros::Publisher> camera_info_pubs;

        deque<boost::mutex> mutex_list;
        vector<sensor_msgs::ImagePtr> img_msgs;
        vector<sensor_msgs::CameraInfoPtr> cam_info_msgs;
        spinnaker_sdk_camera_driver::SpinnakerImageNames mesg;
        boost::mutex queue_mutex_;  
    };

}

#endif
