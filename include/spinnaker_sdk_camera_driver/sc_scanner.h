#include "std_include.h"    //header with most of the ROS stuff

#include "sensor_msgs/Image.h"
#include "spinnaker_sdk_camera_driver/BarcodeResult.h"
#include "spinnaker_sdk_camera_driver/UserAction.h"
#include <Scandit/ScRecognitionContext.h>
#include <Scandit/ScBarcodeScanner.h>

#include <chrono>
#include <numeric>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_surface.h>

class ScScanner {
    public:
        ~ScScanner();
        ScScanner();
        ScScanner(ros::NodeHandle node,ros::NodeHandle private_nh);

        bool service_callback(spinnaker_sdk_camera_driver::UserAction::Request&, spinnaker_sdk_camera_driver::UserAction::Response&);
        void scan_callback(const sensor_msgs::Image&);
        void get_topic_names(const ros::NodeHandle& param_nh, std::vector<std::string>& topics);

        void set_rgb8_image_description();
        void set_mono8_image_description();
        uint32_t m_seq; // sequence number
        std::chrono::seconds rate_report_period;
        int time_avg_size;

    private:
        void _constructor();
        ScRecognitionContext *m_context_;
        ScBarcodeScanner *m_scanner_;
        ScImageDescription *m_image_descr_;
        ScBarcodeScannerSettings *m_settings_;

        ros::NodeHandle m_nh_;
        ros::NodeHandle m_nh_pvt_;
        std::vector<ros::Subscriber> m_image_sub_;
        ros::Publisher m_result_pub_;
        ros::ServiceServer m_debug_service_;

        std::queue<std::chrono::time_point<std::chrono::system_clock>> rate_times;
        std::chrono::time_point<std::chrono::system_clock> last_report_time;

        int m_cam_n_;
};
