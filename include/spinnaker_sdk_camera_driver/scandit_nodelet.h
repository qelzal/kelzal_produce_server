/*#ifndef SPINNAKER_SDK_CAMERA_DRIVER_CAPTURE_NODELET_H
#define SPINNAKER_SDK_CAMERA_DRIVER_CAPTURE_NODELET_H
#endif //SPINNAKER_SDK_CAMERA_DRIVER_CAPTURE_NODELET_H*/

#include <nodelet/nodelet.h>
#include "sc_scanner.h"
#include <Scandit/ScRecognitionContext.h>
#include <Scandit/ScBarcodeScanner.h>
//namespace foo_name {
class scandit_nodelet: public nodelet::Nodelet
{

public:
    scandit_nodelet(){}
    ~scandit_nodelet(){
        if (pubThread_) {
            pubThread_->interrupt();
            pubThread_->join();
        }
    }
    virtual void onInit();

    boost::shared_ptr<ScScanner> inst_;
    std::shared_ptr<boost::thread> pubThread_;

};

//}
