// Define constants
int LightPin     = 2;
int CameraPins[] = {0,1};
int ActiveCamera = 0;
// Definitions waveform generation
int CycleuSec       = 10000;   // 100Hz in uSec
int CameraOnuSec    = 75;
int CameraDelayuSec = 20;
int LightOnuSec     = CameraDelayuSec + CameraOnuSec;
int AllOffuSec      = CycleuSec - LightOnuSec;
void setup() {
  // initialize digital pins as an output.
  pinMode( LightPin,      OUTPUT );
  pinMode( CameraPins[0], OUTPUT );
  pinMode( CameraPins[1], OUTPUT );
  //Initiaze Serial
  Serial.begin(9600);
}
void loop() {
  // Generate Waveform
  Generate();
  
  // check for Parameter Update
  ParameterUpdate();
}
/*
 * Generate Waveforms
 * 
 * Light - Active HIGH
 * Camera1 and 2 - Active HIGH
 * 
 */
void Generate()
{
    // Interval Start - Turn on Lights 
  digitalWrite(LightPin, LOW);
  // Light Ramp Up Time Delay
  delayMicroseconds(CameraDelayuSec);
  // Turn on the Active Camera
  digitalWrite(CameraPins[ActiveCamera], HIGH);
  // Leave Camera on for specified duration
  delayMicroseconds( CameraOnuSec );
  // Turn of Lights and Active Camera
  digitalWrite( LightPin,                 HIGH );
  digitalWrite( CameraPins[ActiveCamera], LOW  );
  // Complete Interval (all off)
  delayMicroseconds( AllOffuSec );
  // Toggle Cameras [0,1]
  ActiveCamera++;
  if ( ActiveCamera > 1 ) ActiveCamera = 0;
}
// Command Processing 
#define BUF_LEN 16
char    Command[BUF_LEN];
void ParameterUpdate()
{
  /* Supported Commands:
     fxxxxx - Set Frequency to xxxxx [Hz]
     exxxxx - Set Exposure Time to xxxxx [microSeconds]
     dxxxxx - Set Delay from Lights on to Exposure on to xxxxx [microSeconds]
     
     note: block until a newline is sent
  */
  int   value;
  char  Command[7];
  String Digits;
  
  // Check for new command string.  Once initiated, pause waveform generation until compelte string is received
  if (Serial.available() > 0 )
  {
    //int num_read = Serial.readBytesUntil('\n', Command, 16);
    String s = Serial.readStringUntil('\n');
    
    // Check for Valid Command
    if ( s.length() != 6 )
    {
      Serial.println("Illegal Command");
    }
    else
    {
      value = (s.substring(1)).toInt();
      // Proceed if we received a valid number
      if ( value > 0 )
      {
        s.toCharArray( Command, 7);
        
        switch( Command[0] )
        {
          case 'f':
            // Convert Frequency [Hz] to Cycle Duration
            CycleuSec = int( 1000000 / value );
  
            // Print Confirmation
            Serial.print("Frequency : ");
            Serial.println(value);
            break;
          
          case 'e':
            // New Exposure duration
            CameraOnuSec = value;
  
            // Print Confirmation
            Serial.print("Exposure : ");
            Serial.println(CameraOnuSec);
            break;
          
          case 'd':
            // New Delay duration
            CameraDelayuSec = value;
  
            // Print Confirmation
            Serial.print("Delay : ");
            Serial.println(CameraDelayuSec);
            break;
          
          default:
            Serial.println("Illegal Command");
            break;
        } 
      }
      // Error
      else Serial.println("Illegal Command");
  
   }
    // Clear String
    s = "";
    
    // Update Waveform Generation Parameters
    LightOnuSec = CameraDelayuSec + CameraOnuSec;
    AllOffuSec  = CycleuSec - LightOnuSec; 
  }
}
