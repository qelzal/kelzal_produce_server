/*

 Mimics the fade example but with an extra parameter for frequency. It should dim but with a flicker 
 because the frequency has been set low enough for the human eye to detect. This flicker is easiest to see when 
 the LED is moving with respect to the eye and when it is between about 20% - 60% brighness. The library 
 allows for a frequency range from 1Hz - 2MHz on 16 bit timers and 31Hz - 2 MHz on 8 bit timers. When 
 SetPinFrequency()/SetPinFrequencySafe() is called, a bool is returned which can be tested to verify the 
 frequency was actually changed.
 
 This example runs on mega and uno.
 */
#define BUF_LEN 16
#include <PWM.h>

//use pin 11 on the Mega instead, otherwise there is a frequency cap at 31 Hz
int outPin = 9;                // the pin that the LED is attached to
int frequency = 50;        //frequency (in Hz) - timer 1 frequency - timer 1 is 16 bit
int exposure = 100;   // exposure time in us
char buf[BUF_LEN];

void setup()
{
  Serial.begin(115200);
  //initialize all timers except for 0, to save time keeping functions
  InitTimersSafe(); 
  pinMode(outPin, OUTPUT);
  //sets the frequency for the specified pin
   
  if(true) {// turn led on
    pinMode(13, OUTPUT);
    digitalWrite(13, HIGH);
  }
  set_freq();
  set_exposure();
}

void loop()
{
  if (Serial.available() > 0) {
    int num_read = Serial.readBytesUntil('\n', buf, 16);
    int num = -1;
    if (num_read < 16 && num_read > 0) {
      if (buf[0] == 'f')  {
        num = get_num(buf+1, 5);
        if (num > 0)  {
          frequency = num;
          if (set_freq()) {
            set_exposure();
            Serial.print("f");
            Serial.println(num);  
          }
        }
        else  {
          Serial.println("Error");
        }
      }
      else if (buf[0] == 'e') {
        num = get_num(buf+1, 5);
        exposure = num;
        set_freq();
        set_exposure();
        Serial.print("e");
        Serial.println(num);
      }
    }
    else  {
      Serial.println("No Command");
      clear_buf();
    }
  }
  
//  pwmWriteHR(outPin, 364); // /Duty Cycle wanted for 50Hz is 0.005 - for 16bit 65536 * 0.005 = ~328
}

int get_num(char* buf, int len) {
  //0 to 65535
  int ret_val = 0;
  for (int i=0; i<len; i++) {
    if (buf[i]<48 || buf[i]>57) {
      return -1;
    }
    
    int this_val = (buf[i]-48);
    for (int p=0; p<len-1-i; p++) {
      this_val *= 10;
    }

    ret_val += this_val;
  }

  return ret_val;
}

void clear_buf()  {
  for (int i=0; i<BUF_LEN; i++) {
    buf[i] = 0;
  }
}

bool set_freq() {
  bool success1 = SetPinFrequencySafe(outPin, frequency);
  return success1;
}

void set_exposure() {
  double period = 1.0/frequency;
  double duty = exposure / (period * 1e6);
  Serial.println((uint16_t)(-1));
  if (duty < 0.5) {
    uint16_t val = 65536 * duty;
    pwmWriteHR(outPin, val); // Duty Cycle wanted for 50Hz is 0.005 - for 16bit 65536 * 0.005 = ~328
  }
}
