(function ($) {
    "use strict";



    // background image - start
    // --------------------------------------------------
    $('[data-background]').each(function () {
        $(this).css('background-image', 'url(' + $(this).attr('data-background') + ')');
    });
    // background image - end
    // --------------------------------------------------



    // menu button - start
    // --------------------------------------------------
    $(document).ready(function () {
        $('.close-btn, .overlay').on('click', function () {
            $('#cart-sidebar').removeClass('active');
            $('.overlay').removeClass('active');
            console.log("Close Button Clicked!!");
        });

        $('.cart-btn').on('click', function () {
            $('#cart-sidebar').addClass('active');
            $('.overlay').addClass('active');
        });
    });

    $('.menu-open').click(function () {
        $('.main-menu').addClass('active');
        console.log("menu open button clicked!");
    });
    $('.menu-close').click(function () {
        $('.main-menu').removeClass('active');
        console.log("menu-close button clicked!");
    });
    // menu button - end
    // --------------------------------------------------

    // item x button - start
    // --------------------------------------------------
    
    
    // item x button - end
    // --------------------------------------------------

    // sticky menu - start
    // --------------------------------------------------
    var headerId = $(".sticky-header");
    $(window).on('scroll', function () {
        var amountScrolled = $(window).scrollTop();
        if ($(this).scrollTop() > 60) {
            headerId.removeClass("not-stuck");
            headerId.addClass("stuck");
        } else {
            headerId.removeClass("stuck");
            headerId.addClass("not-stuck");
        }
    });
    // sticky menu - end
    // --------------------------------------------------



})(jQuery);
