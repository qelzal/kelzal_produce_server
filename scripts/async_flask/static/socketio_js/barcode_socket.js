$(document).ready(function(){
    //connect to the socket server.flask socketio javascript socket.on not working
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/socket');
    console.log(document.domain);
    console.log(location.port);
    console.log(socket);
    console.log('We are running the correct script');
    var initial_load = 1;
    
    //receive details from server
    
    

    function add_product(update) {
      
        sku = update.sku.toString();
        id = 'photo_' + sku;
        quantity = parseInt(update.number, 10);
        unit_price = update.unit_price;
        total_product_price = '$' + (quantity * unit_price).toFixed(2).toString();     
        unit_price_str = '$' + unit_price.toString();         
        total_price = update.total_price;
        total_price_str = '$' + total_price.toString();
        product_name = update.product_name.toString();
        console.log('add_product adding:',product_name);
        quantity_price_str = quantity.toString() + 'x' + unit_price_str;   
        $( "#" + id ).remove();
        $("#sku" + sku).remove();
        if (quantity > 0) {
           console.log('Adding element in add_update funtion');
           var row_photo_elem_start = `<div id=${id} class="small-product bg-white clearfix"><button type="button" class="remove-btn" onclick="console.log('User wants to remove item!'); remove_one_product(${sku});"><i class="fal fa-times-circle"></i></button><div class="row"><div class="col-lg-4 col-md-4 col-sm-12"><div class="image-container">`
           var photo_path = `<img src="static/images/products/${sku}.jpg" alt="Image Not Found"  onerror="this.onerror=null; this.src='static/images/products/smiley.jpeg';">`;
           console.log(photo_path);
           var row_photo_elem_end = `</div></div><div class="col-lg-4 col-md-4 col-sm-12"><div class="item-content ul-li-block clearfix"><h3>${product_name}</h3><span>${quantity_price_str}</span>`;
           var row_photo_elem_end2 = `<br /><br /><strong>${total_product_price}</strong></div></div><div class="col-lg-4 col-md-4 col-sm-12 d-flex align-items-center"></div></div>`;
           var row_photo_elem_end3 = `<div class="btns-group ul-li-block clearfix"></div></div>`;
           total_photo_tag = row_photo_elem_start + photo_path + row_photo_elem_end + row_photo_elem_end2 + row_photo_elem_end3;
           $( ".products-wrapper" ).prepend(total_photo_tag);
           console.log($( ".products-wrapper" ).length); 
           var tag_for_right_side_inventory = `<div id="sku${sku}" class="cart-item text-white ul-li clearfix"><h4>${product_name}</h4><ul class="clearfix"><li>` +
               `${quantity_price_str}</li></ul></div>`;
           console.log(tag_for_right_side_inventory);
           //$("#first_checkout_elem").after(tag_for_right_side_inventory);
	         $("#checkout_item_list").append(tag_for_right_side_inventory);
        }
        $("#total_price").html(total_price_str);
        if  ($('[id^=photo_]').length == 0){
           $("#total_price").html('$0.00');
        }

    }

    function do_list_reset(msg) {
        $("#total_price").html('$0.00');
        $('div[id^="sku"]').remove();
        $('div[id^="photo"]').remove();
        console.log('HERE');
        num_all_updates = msg.all_updates.length;
        console.log(num_all_updates);
        console.log('Reset received');
        if (initial_load == 1){ 
          initial_load = 0;
          console.log('INITIAL UPDATE');
        } 
        for (var i = 0; i < num_all_updates; i++) {
          update = msg.all_updates[i];   
          console.log('After reset, updating ...');
          console.log(update);
          add_product(update);
        }
    }
    
    
    function make_incremental_list_update(msg){
        num_all_updates = msg.all_updates.length;
        if (initial_load == 1){ 
          initial_load = 0;
          console.log('INITIAL UPDATE');
          for (var i = 0; i < num_all_updates; i++) {
             update = msg.all_updates[i];   
             add_product(update);
          }
        }
        else {
           update = msg.new_update;
           add_product(update);
        }
    }
    socket.on('barcode', function(msg) {
        console.log(msg);    
        
        if (msg.event == 'reset') {
          do_list_reset(msg);
        }
        else {
          make_incremental_list_update(msg);
        }
    });

    


});

var socket = io.connect('http://' + document.domain + ':' + location.port + '/socket');

function remove_one_product(sku) {
  console.log("In Remove One Product Function!:Product id:", sku);
  socket.emit('remove_one_item', {'sku': sku});
}
