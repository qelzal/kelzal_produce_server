




$(document).ready(function(){
    //connect to the socket server.
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/fruit/socket');
    $("#produce_detected_tag").remove();
    var skus_received = {};
    var fruit_currently_detected = 0;    
    var weight = 0;
    var unit_price = 0;
    var total_price = 0;
        

    function play_beep(){
        var audio = new Audio('../static/audio/beep-07.wav');
        audio.play();
    }


    function produce_type_tag(produce, price) {
         console.log(price);
         combined_str = produce.replace('_', ' ')  + '     ' + price; 
         line1 = `<div id="produce_detected_tag" class="checkout-item ul-li  mb-30 clearfix">`;
         line2 = `<h3 class="bg-default-purple" style="color:white;">${combined_str}</h3>`;
         line3 = `<ul class="clearfix">`;
         line4 = `</li></ul></div>`;
         combined_str = line1 + line2 + line3 + line4;
         console.log(combined_str);
         return combined_str;
    }

    function get_total_price(weight, unit_price){
        var total_price = (weight * unit_price);
        total_price = Math.round(total_price * 100) / 100;
        return total_price;
    }


    //receive details from server
    socket.on('fruit', function(msg) {
        if (msg.event == 'new_fruit') {
           $("#produce_detected_tag").remove();
           console.log(msg);
           unit_price = msg.unit_price;
           unit_price_str = '$' + unit_price.toString();         
           total_price = get_total_price(weight, unit_price);
           total_price_str = '$'+total_price.toString();
           product_name = msg.product_name.toString();
           if (product_name != 'null'){
              fruit_currently_detected = 1;
              play_beep();
              $("#count").html(weight);
              file_name = `${product_name}.jpg`;
              console.log(unit_price_str);
              produce_tag = produce_type_tag(product_name.toUpperCase(), unit_price_str + '/lb.');
              $("#produce_type_container").append(produce_tag);    
           }
           else {
              fruit_currently_detected = 0;
              file_name = "produce_basket.png";
              $("#produce_detected_tag").remove();
              $("#count").html("0 lbs.");
           }
           console.log(product_name)
           image = `../static/images/produce/` + file_name;       
           $("#breadcrumb-section").attr("data-background", image);
           $("#breadcrumb-section").attr("style", `background-image: url("../static/images/produce/${file_name}")`);
           $("#total_price").html(total_price_str);
           $("#unit_price").html(unit_price_str + '/lb.' );
        }
        else {
           weight = msg.weight
           weight_str = msg.weight.toString() + 'lbs.';
           if (fruit_currently_detected == 1) {
              $("#count").html(weight_str);                            
              total_price = get_total_price(weight, unit_price);
              total_price_str = '$'+total_price.toString();
              $("#total_price").html(total_price_str);
           } 
           else {
              $("#count").html("0 lbs.");
           }
        }
    });


});



