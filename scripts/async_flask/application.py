#!/usr/bin/env python3


# Start with a basic flask app webpage.
import os
import sys
from flask_socketio import SocketIO, emit
from spinnaker_sdk_camera_driver.srv import UserAction, UserActionResponse
from flask import Flask, render_template, url_for, \
 copy_current_request_context, redirect
from std_msgs.msg import String
# sys.path.append('~/catkin_ws/src/')
# sys.path.append('~/catkin_ws/src/kelzal_produce_server/')
from multiprocessing import Process
import rospy
import rospkg
from kelzal_produce_server.msg import CartContents as CartContentsMsg
from kelzal_produce_server.msg import ProduceScale as ProduceScaleMsg
import json
#from random import random
from time import sleep
from threading import Thread, Event, Timer #Sam Added Timer
import random

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = True

rospack = rospkg.RosPack()
produce_path = rospack.get_path('kelzal_produce_server')
spinnaker_path = rospack.get_path('spinnaker_sdk_camera_driver')
PRODUCT_IMAGES_DIR = os.path.join(produce_path, 'scripts/async_flask/assets/images/products')
MAJ_MKT_PRODUCTS_FILE = os.path.join(spinnaker_path,'params/002168EANItems.csv')
FOREIGN_PRODUCTS_FILE = None if len(sys.argv) > 1 else sys.argv[1]

class ProductList():

    def __init__(self, product_file, images_dir, foreign_product_file = None):
       clean = lambda x: x.lstrip().rstrip()
       self.product_dict = {}     
       with open(product_file, 'r', errors='ignore') as product_file_handle:
           product_list = product_file_handle.readlines()
       product_list = [line.split(',') for line in product_list]
       for line_num, line_tuple in enumerate(product_list):
           try:
                barcode = line_tuple[0] 
                name = clean(line_tuple[1])
                weight = 0.0  #float(clean(line_tuple[2]))
                price = float(clean(line_tuple[3]))

           except ValueError as e:
                print(('Invalid format encountered in line %d of the products file. Line will ' % line_num) +
                       'be ignored in forming the server inventory list.') 
                print('Error message: %s' % str(e))
           self.product_dict[barcode] = {
                                         'name': name,
                                         'weight': weight,
                                         'price': price
                                        } 
       if foreign_product_file:
           self.add_supplemental_product_info(foreign_product_file)
       self.impose_cart_reset()
       self.current_fruit = None
       self.produce_prices = {'banana': 1.99,
                              'kiwi': 2.99,
                              'lemon': 2.49,
                              'lime': 1.79,
                              'orange': 2.79,
                              'pomegranate': 1.99,
                              'red_apple': 2.59
                             }


    def add_supplemental_product_info(self, foreign_products_file):
       with open(foreign_products_file, 'r') as foreign_product_file_handle:
           foreign_product_list = foreign_product_file_handle.readlines()
       foreign_product_list = [line.split(',') for line in foreign_product_list]
       for line in foreign_product_list:
           try:
              barcode = line[0]
              num_missing_ean13_leading_zeros = 13 - len(barcode)
              leading_zeros = ''.join(["0" for _ in range(num_missing_ean13_leading_zeros)])
              barcode = leading_zeros + barcode
              name = line[1]
              if barcode not in self.product_dict:
                 self.product_dict[barcode] = {
                                              'name': clean(name),
                                              'weight': 0.0,
                                              'price': 0.0
                                              } 
           except:
              print('Invalid format encountered while reading line in product CSV file.')


    
    def update_barcode_list(self, current_list_reported_by_ROS):
       self.barcode_list = current_list_reported_by_ROS 


    def verify_no_reset(self, changed_barcode, quantity_change, state_ctrl_barcode_list):
       """
       Function that compares the list of barcodes in the cart, as maintained by the server,
       with the list of barcodes in the cart being reported by the cart contents ROS node.  
       If the 2 lists are the same, this indicates that no cart contents resetting/emptying
       has occurred. If the 2 lists are different, then this indicates that the the cart 
       contents node has reset the cart contents, and the server must rebuild its list of 
       cart contents based on the list being reported by cart contents and refresh the 
       webpage javascript/html display list of products in its entirety.  
       """
       if quantity_change == 0:
          barcode_list_w_newest_change = sorted(self.barcode_list) 
       if quantity_change == 1:
          barcode_list_w_newest_change =  sorted(self.barcode_list + [changed_barcode])  
       if quantity_change == -1:
          if changed_barcode in self.barcode_list:     
             self.barcode_list.remove(changed_barcode)
             barcode_list_w_newest_change =  sorted(self.barcode_list)
          else:
             return False
       return barcode_list_w_newest_change == sorted(state_ctrl_barcode_list)


    def impose_cart_reset(self):
       """
       Used to reinitialize the cart contents tracking done by this class when the result 
       of the verify_no_reset function indicates that a reset of the cart contents has occurred.
       """ 

       self.barcode_list = []
       self.total_price = 0
       self.products_in_cart = {key: 0 for key in self.product_dict.keys()}
       self.all_product_update_reports = []


    def add_product_to_cart(self, sku):
       print('Adding to cart')
       if sku not in self.products_in_cart:
          self.products_in_cart[sku] = 1
       else:
          self.products_in_cart[sku] += 1
       if sku in self.product_dict:
          self.total_price += self.product_dict[sku]['price'] 
       

    def add_new_product_report(self, new_product_report):
       self.all_product_update_reports.append(new_product_report)

       
    def remove_product_from_cart(self, sku):
       print('Removing from cart')
       if sku in self.product_dict:
          price = self.product_dict[sku]['price']
          self.total_price -= float(price)
       self.products_in_cart[sku] -= 1


def serve_barcode_and_fruit_data():

    def barcode_callback(msg, product_list_obj):
         
          def make_report(sku):
             
             if sku in product_list_obj.product_dict:
                product_name = product_list_obj.product_dict[sku]['name']
                price = round(product_list_obj.product_dict[sku]['price'], 2)
                print("found price is '{}' of type {}".format(price, type(price)))
             else:
                product_name = 'Unkown Product - Barcode %s' %sku
                price = 0.0
             report = {
                      'product_name': product_name,
                      'sku': sku,
                      'number': product_list_obj.products_in_cart[sku],
                      'unit_price': price,
                      'total_price': round(product_list_obj.total_price, 2)
                     }
             print('Generating report: '+ str(report))
             return report 

          print('Message received :' + str(msg))
          product_list_obj.initial = 0
          product_sku = msg.item_change
          change = int(msg.num_change)
          contents_list = msg.contents_list
          print(product_sku, change, contents_list)
          cart_was_reset = product_list_obj.verify_no_reset(product_sku, change, contents_list) 
          if not cart_was_reset and change > 0:
             if change == 1:
                product_list_obj.add_product_to_cart(product_sku)
             print('In barcode callback: ' + product_sku)
             new_product_report = make_report(product_sku)
             product_list_obj.add_new_product_report(new_product_report)
             print('Server emitting new barcode detection ***********')
             socketio.emit('barcode', 
                      {
                       'event': 'new_product_added',
                       'new_update': new_product_report,
                       'all_updates': product_list_obj.all_product_update_reports 
                      },
                      namespace='/socket')
          else:
              product_list_obj.impose_cart_reset() #make server's tracker reflect the reset
              for sku in contents_list: #now add all contents being reported
                 product_list_obj.add_product_to_cart(sku)
                 new_product_report = (make_report(sku))
                 product_list_obj.add_new_product_report(new_product_report)
              print('Server emitting reset****************8')
              socketio.emit('barcode',
                            {
                             'event': 'reset',
                             'all_updates': product_list_obj.all_product_update_reports
                            },
                            namespace = '/socket')
          product_list_obj.update_barcode_list(contents_list)
          socketio.sleep(.01) 


    def produce_callback(msg, product_list_obj):
          fruit_type = str(msg.data).lower()
          if fruit_type == 'null':
             price = 0
          else:
             price = product_list_obj.produce_prices[fruit_type]
          print('FRUIT COMMUNICATED TO SERVER: ' + str(fruit_type))
          socketio.emit('fruit', 
                      { 
                        'event': 'new_fruit',
                        'product_name': fruit_type, 
                        'unit_price': round(price, 2)
                      },
                      namespace='/fruit/socket')
          product_list_obj.current_fruit = fruit_type
          socketio.sleep(.1)


    def produce_scale_callback(msg, product_list_obj):
          weight = round(float(msg.weight), 2)
          current_fruit = product_list_obj.current_fruit
          if current_fruit and current_fruit.lower() is not 'null':
             socketio.emit('fruit', 
                           {
                            'event': 'weight', 
                            'weight': weight
                           }, 
                           namespace='/fruit/socket')


    product_list_obj = ProductList(MAJ_MKT_PRODUCTS_FILE, PRODUCT_IMAGES_DIR, FOREIGN_PRODUCTS_FILE)
    rospy.init_node('Item_List_Server', disable_signals=True)
    rospy.Subscriber('/fruit', String, produce_callback, product_list_obj)
    rospy.Subscriber('/cart_contents', CartContentsMsg, barcode_callback, product_list_obj)
    rospy.Subscriber('/produce_scale_weight', ProduceScaleMsg, produce_scale_callback, product_list_obj)
    socketio.sleep(10)
    service_proxy = rospy.ServiceProxy('/state_ctrl/UserInput', UserAction)
    service_proxy('get_contents')


def command_reset(): #delete this once verified that ROS_command replicates functionality
    service_proxy = rospy.ServiceProxy('/state_ctrl/UserInput', UserAction)
    service_proxy('hard_reset')


#turn the flask app into a socketio app
socketio = SocketIO(app, async_mode=None, logger=True, engineio_logger=True)
barcode_thread = socketio.start_background_task(serve_barcode_and_fruit_data)
barcode_thread_stop_event = Event()
socketio.sleep(10)

print("After Socketio App Creation!") #For Debugging





# App Routes ------------------------------------------------------------------------
@app.route('/')
def index():
    #only by sending this page first will the client be connected to the socketio instance
    return render_template('barcode.html', async_mode=socketio.async_mode)


@app.route('/fruit/')
def serve_checkout_page():
    return render_template('fruit.html')

@app.route('/reset')
def reset():
    reset_process = Process(target=command_reset)
    reset_process.start()
    reset_process.join()
    return redirect(url_for('index'))

@app.route('/stop')
def clearscreen():
   print("Stop session button pressed!") # For Debugging
   clearscreen_service_proxy = rospy.ServiceProxy('/server_message_publisher/ServerMessage', UserAction)
   clearscreen_service_proxy('Stop session')
   return redirect(url_for('index')) #Possibly this should be changed?

@app.route('/start')
def sessiontoggle():
   print("Start session button pressed!") #For debugging
   session_toggle_service_proxy = rospy.ServiceProxy('/server_message_publisher/ServerMessage', UserAction)
   session_toggle_service_proxy('Start Session') 
   return redirect(url_for('index')) #Possibly this should be changed?

#Socket IO Definitions ------------------------------------------------------
@socketio.on('connect', namespace='/socket')
def barcode_socket_connect():
    # need visibility of the global thread object
    global barcode_thread
    service_proxy = rospy.ServiceProxy('/state_ctrl/UserInput', UserAction)
    service_proxy('get_contents')

@socketio.on('connect', namespace='/fruit/socket')
def fruit_socket_connect():
    # need visibility of the global thread object
    global barcode_thread
    print('Client connected')

@socketio.on('disconnect', namespace='/socket')
def barcode_socket_disconnect():
    print('Client disconnected')

@socketio.on('disconnect', namespace='/fruit/socket')
def fruit_socket_disconnect():
    print('Client disconnected')

@socketio.on('remove_one_item', namespace='/socket')
def remove_one_item(sku):
    remove_item_service_proxy = rospy.ServiceProxy('/server_message_publisher/ServerMessage', UserAction)
    sku_to_remove = sku['sku']
    remove_item_service_proxy('Remove %s' %sku_to_remove) 
    print("RECIEVED MESSAGE: User wants to remove one item: %s" % sku_to_remove)






if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=5000)
