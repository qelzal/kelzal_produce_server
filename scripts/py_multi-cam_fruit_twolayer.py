#!/usr/bin/env python3
# run to get python3 for ROS:
# sudo apt-get install python3-rospkg-modules
import cv2
from collections import Counter
import time
import os
print(os.getcwd())
import rospy
from std_msgs.msg import String
######################### Loading the fruit model
import numpy as np
from tensorflow.keras import backend as K
from tensorflow.keras.models import Sequential,Model
from tensorflow.keras.layers import Activation, Flatten, Dense

from choose_model import get_model
#import tensorflow as tf
#from keras.backend.tensorflow_backend import set_session

K.set_learning_phase(0)

class Prediction():
    """
    Predicting the class of given image
    """
    def __init__(self,model_file_path, num_classes=8):
        """
        Initializes the prediction class object
        """
        self.label_map = {'0':'banana', '1':'kiwi', '2':'lemon', '3':'lime', '4':'null', '5':'orange', '6':'pomegranate', '7':'red_apple'}
        self.model_file_name = model_file_path.split('/')[-1]
        print(model_file_path.split('/'))
        print(self.model_file_name)
        self.basemodel_name = self.model_file_name.split('_')[-3]
        print("BASEMODEL name: ",self.basemodel_name)
        self.basemodel = get_model(self.basemodel_name,include_top=False, weights='imagenet')
        basemodel_features_0 = self.basemodel.predict(np.zeros((1,224,224,3)))
        self.finetuned_model = Sequential()
        self.finetuned_model.add(Flatten(input_shape=basemodel_features_0.shape[1:]))
        self.finetuned_model.add(Dense(256, activation='relu'))
        self.finetuned_model.add(Dense(num_classes, activation="softmax"))
        self.finetuned_model.load_weights(model_file_path)


    def predict(self,input_img):
        """
        Predict the class of the given image
        """
        # Compute the features from the basemodel
        input_img = input_img/255.

        basemodel_features = self.basemodel.predict(input_img)
        predicted_label = self.finetuned_model.predict(basemodel_features)
        return predicted_label

def image_centeralize(cam_id, img, size=(224,224)):
    y_c, x_c = img.shape[:2]
    if cam_id == 1:
        y_c = int(y_c/2)-60
        x_c = int(x_c/2)+106
    if cam_id == 0:
        y_c = int(y_c/2)+40
        x_c = int(x_c/2)-110
    if cam_id == 2:
        y_c = int(y_c/2)
        x_c = int(x_c/2)+40
    if cam_id == 3:
        y_c = int(y_c/2)-50
        x_c = int(x_c/2)-146

    return img[y_c - int(size[1]/2):y_c + int(size[1]/2),x_c - int(size[0]/2):x_c + int(size[0]/2)]
#########################

# Create a list of cameras
#
No_of_cams = 4
cam_list = [cv2.VideoCapture() for i in range(No_of_cams)]
gst_str = ["" for x in range(No_of_cams)]

# Camera setup
#
gst_str[0] = ('nvarguscamerasrc sensor-id=_TBD, exposurecompensation=-2 ! '
                   'video/x-raw(memory:NVMM), '
                   'width=(int)640, height=(int)480, '
                   'auto-exposure=(int)1, exposure-time=0.001, '
                   'format=(string)NV12, framerate=(fraction)30/1, wbmode=6 ! '
                   'nvvidconv flip-method=0 ! '
                   'video/x-raw, width=(int){}, height=(int){}, '
                   'format=(string)BGRx ! '
                   'videoconvert ! appsink').format(640, 480)
                   
gst_str[1] = ('nvarguscamerasrc sensor-id=_TBD ! '
                   'video/x-raw(memory:NVMM), '
                   'width=(int)640, height=(int)480, '
                   'auto-exposure=(int)1, exposure-time=0.001, '
                   'format=(string)NV12, framerate=(fraction)30/1, wbmode=6 ! '
                   'nvvidconv flip-method=0 ! '
                   'video/x-raw, width=(int){}, height=(int){}, '
                   'format=(string)BGRx ! '
                   'videoconvert ! appsink').format(640, 480)

gst_str[2] = ('nvarguscamerasrc sensor-id=_TBD ! '
                   'video/x-raw(memory:NVMM), '
                   'width=(int)640, height=(int)480, '
                   'auto-exposure=(int)1, exposure-time=0.001, '
                   'format=(string)NV12, framerate=(fraction)30/1, wbmode=6 ! '
                   'nvvidconv flip-method=0 ! '
                   'video/x-raw, width=(int){}, height=(int){}, '
                   'format=(string)BGRx ! '
                   'videoconvert ! appsink').format(640, 480)
                   
gst_str[3] = ('nvarguscamerasrc sensor-id=_TBD ! '
                   'video/x-raw(memory:NVMM), '
                   'width=(int)640, height=(int)480, '
                   'auto-exposure=(int)1, exposure-time=0.001, '
                   'format=(string)NV12, framerate=(fraction)30/1, wbmode=6 ! '
                   'nvvidconv flip-method=0 ! '
                   'video/x-raw, width=(int){}, height=(int){}, '
                   'format=(string)BGRx ! '
                   'videoconvert ! appsink').format(640, 480)
                   
for cam_id in range(No_of_cams):
    print ("INFO:  Setting up CAP_GSTREAMER...")

    # Start cameras using GSTRAMER
    #
    gst_str_cam_id = gst_str[cam_id].replace("_TBD", str(cam_id), 1)
    cam_list[cam_id] =  cv2.VideoCapture(gst_str_cam_id, cv2.CAP_GSTREAMER)
    if cam_list[cam_id].isOpened():
        print ("INFO:  Fruit Camera " + str(cam_id) + " is up...")
    else:
        print ("ERROR: Fruit Camera " + str(cam_id) + " failed to open.")

    # Image configuration
    #
    print ("INFO : Setting Fruit Camera " + str(cam_id) + " configuration...")
    # TBD: the following configuration code is causing core dump
    #cam_list[cam_id].set(cv2.CAP_PROP_FRAME_WIDTH, 224)
    #cam_list[cam_id].set(cv2.CAP_PROP_FRAME_HEIGHT, 224)

    #cam_list[cam_id].set(cv2.CAP_PROP_FRAME_WIDTH, 2594)
    #cam_list[cam_id].set(cv2.CAP_PROP_FRAME_HEIGHT, 1964)
    #cam_list[1].set(cv2.CAP_PROP_EXPOSURE, 0.6)

#config = tf.ConfigProto()
#config.gpu_options.allow_growth = True
#set_session(tf.Session(config=config))

weight_file = rospy.get_param('weight_file', None)
debug_mode = rospy.get_param('fruit_debug', False)

model_file = weight_file
image_list = [0] * 4
pr = Prediction(model_file)
print('cam_id: 0   1   2   3', '\n' )
rospy.init_node('produce_publisher', anonymous=True)
pub = rospy.Publisher('fruit', String, queue_size=10)
prev_label = "null"
buffer_len = 20
buffer_list = ["null"] * buffer_len
idx = 0
while not rospy.is_shutdown():
    strt = time.time()
    for cam_id in range(No_of_cams):
        # Retrieve a frame from each camera
        ret1, img = cam_list[cam_id].read()
        #print("size of the captured img: ",img.shape)
        img = image_centeralize(cam_id, img)
        if debug_mode:
            cv2.imshow('Cam ' + str(cam_id), img)
        
        image_list[cam_id] = img

    inp_image = np.concatenate((image_list[0][np.newaxis, :, :, :], image_list[1][np.newaxis, :, :, :], image_list[2][np.newaxis, :, :, :], image_list[3][np.newaxis, :, :, :]), axis=0)
    
    pred = pr.predict(inp_image)
    pred_sum = .25*pred[0] + .25*pred[1] + .25*pred[2] + .25*pred[3]
    if np.argmax(pred_sum) == 0 and pred_sum[np.argmax(pred_sum)] > 0.75:
        pred_label = pr.label_map[str(np.argmax(pred_sum))]
    elif np.argmax(pred_sum) == 1 and pred_sum[np.argmax(pred_sum)] > 0.4:
        pred_label = pr.label_map[str(np.argmax(pred_sum))]
    elif np.argmax(pred_sum) == 2 and pred_sum[np.argmax(pred_sum)] > 0.5:
        pred_label = pr.label_map[str(np.argmax(pred_sum))]
    elif np.argmax(pred_sum) == 3 and pred_sum[np.argmax(pred_sum)] > 0.25:
        pred_label = pr.label_map[str(np.argmax(pred_sum))]
    elif np.argmax(pred_sum) == 5 and pred_sum[np.argmax(pred_sum)] > 0.4:
        pred_label = pr.label_map[str(np.argmax(pred_sum))]
    elif np.argmax(pred_sum) == 6 and pred_sum[np.argmax(pred_sum)] > 0.4:
        pred_label = pr.label_map[str(np.argmax(pred_sum))]
    elif np.argmax(pred_sum) == 7 and pred_sum[np.argmax(pred_sum)] > 0.75:
        pred_label = pr.label_map[str(np.argmax(pred_sum))]
    else:
        pred_label = "null"
    buffer_list[idx % buffer_len] = pred_label
    print('######', buffer_list)
    idx += 1
    occurence_count = Counter(buffer_list) 
    pred_label = occurence_count.most_common(1)[0][0]   
    # Write to the shared_list
    if pred_label!=prev_label:
        pub.publish(pred_label)
        prev_label = pred_label
    if debug_mode:
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break
    

# Camera clean-up
#
for cam_id in range(No_of_cams):
    print("INFO:  cam_id = " + str(cam_id) + " being released.")
    cam_list[cam_id].release()
    del(cam_list[cam_id])

cv2.waitKey(0)
cv2.destroyAllWindows()
