from keras_applications.densenet import DenseNet121, DenseNet169,DenseNet201
from keras_applications.efficientnet import EfficientNetB0, EfficientNetB1, EfficientNetB2, EfficientNetB3, EfficientNetB4, EfficientNetB5, EfficientNetB6, EfficientNetB7
from keras_applications.inception_resnet_v2 import InceptionResNetV2
from keras_applications.inception_v3 import InceptionV3
from keras_applications.mobilenet_v2 import MobileNetV2
from keras_applications.mobilenet import MobileNet
from keras_applications.nasnet import NASNetLarge,NASNetMobile
from keras_applications.resnet import ResNet50, ResNet101, ResNet152
from keras_applications.resnet_v2 import ResNet50V2, ResNet101V2, ResNet152V2
from keras_applications.resnext import ResNeXt50, ResNeXt101
from keras_applications.vgg16 import VGG16
from keras_applications.vgg19 import VGG19
from keras_applications.xception import Xception
import tensorflow.keras as keras


def get_model(model,include_top = False, weights="imagenet"):
    model = model.lower()
    # dense nets
    if model == 'densenet121':
        return DenseNet121(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'densenet169':
        return DenseNet169(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'densenet201':
        return DenseNet201(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)

    # efficient nets
    if model == 'efficientnetb0':
        return EfficientNetB0(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'efficientnetb1':
        return EfficientNetB1(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'efficientnetb2':
        return EfficientNetB2(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'efficientnetb3':
        return EfficientNetB3(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'efficientnetb4':
        return EfficientNetB4(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'efficientnetb5':
        return EfficientNetB5(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'efficientnetb6':
        return EfficientNetB6(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'efficientnetb7':
        return EfficientNetB7(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)

    if model == 'inceptionresnetv2':
        return InceptionResNetV2(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'inceptionv3':
        return InceptionV3(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)

    if model == 'mobilenetv2':
        return MobileNetV2(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'mobilenet':
        return MobileNet(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)

    # nas nets
    if model == 'nasnetmobil':
        return NASNetMobile(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'nasnetlarge':
        return NASNetLarge(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)

    # resnet
    # v1
    if model == 'resnet50':
        return ResNet50(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'resnet101':
        return ResNet101(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'resnet152':
        return ResNet152(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    # v2
    if model == 'resnet50v2':
        return ResNet50V2(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'resnet101v2':
        return ResNet101V2(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'resnet152v2':
        return ResNet152V2(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    # next
    if model == 'resnext50':
        return ResNeXt50(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'resnext101':
        return ResNeXt101(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)

    if model == 'vgg16':
        return VGG16(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'vgg19':
        return VGG19(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)
    if model == 'xception':
        return Xception(include_top=include_top, weights=weights,backend=keras.backend,layers=keras.layers, models=keras.models, utils=keras.utils)

    assert(1,"model not found")
