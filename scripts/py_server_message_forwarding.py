#!/usr/bin/env python3
from std_msgs.msg import String
import rospy
from spinnaker_sdk_camera_driver.srv import UserAction
from spinnaker_sdk_camera_driver.srv import UserActionResponse

def message_callback(user_action_req):
    global publisher
    action_requested = str(user_action_req.action)
    publisher.publish(action_requested)
    return UserActionResponse('some_response')


def run_services():
    forwarding_service = rospy.Service("/server_message_publisher/ServerMessage", UserAction, message_callback)


if __name__ == "__main__":
    rospy.init_node('server_message_publisher')
    publisher = rospy.Publisher('server_message', String, queue_size=10)
    run_services()
    rospy.spin()

