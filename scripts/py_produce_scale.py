#!/usr/bin/env python3
# run to get python3 for ROS:
# sudo apt-get install python3-rospkg-modules
import sys
print(sys.version_info)

import pyudev
import serial
import time

import rospy
from kelzal_produce_server.msg import ProduceScale as ProduceScaleMsg

tare = 0

class ProduceScale:
    def __init__(self):
        self.counter = 0

        self.pubs = {}
        self.pubs['weight'] = rospy.Publisher('produce_scale_weight', ProduceScaleMsg, queue_size=5)

        scale_sns = rospy.get_param("scale_sn", default=[])
        print(scale_sns)
        if isinstance(scale_sns, str):  #check if there is only a singular serial number
            scale_sns = [scale_sns]
        print("using serial numbers:", scale_sns)
        context = pyudev.Context()
        for device in context.list_devices(subsystem='tty'):
            if ('ID_SERIAL_SHORT' in device):# and (int(device['USEC_INITIALIZED']) in scale_sns):
                dev_name = device.device_node
                self.ser = serial.Serial(dev_name)
                print(self.ser.name)
                print('Found and using device: {}'.format(dev_name))

        rospy.init_node('produce_scale')
        rospy.Timer(rospy.Duration(1.0/200.0), self.ArivaPoll)
        rospy.spin()

    def ArivaPoll(self, timer):

      self.counter += 1
      # Poll Ariva Scale until a valid weight is decoded
      while True:

        # Initialize
        char = ""
        ArivaString = bytearray()

        #Send poll character to Scale
        self.ser.write(b'W')
        # Read one character at a time until a termination character is found ('\r')
        while char != b'\r':
            char = self.ser.read()

            # construct message, ignoring the first byte (STX = 0x02) and the last byte (CR = 0x0D)
            if (char != b'\x02') and (char != b'\r'):
                ArivaString += char

        # Check if the received string starts with the Status Byte ( indicated by a '?' or a value of 63 )
        if ArivaString[0] == 63:
            # Parse the Status Byte
            status = ArivaString[1]

            ScaleInMotion      = status & 0x1     # bit 0 : In Motion / Stable
            ScaleOverCapacity  = status & 0x2     # bit 1 : OverCapacity / In Range
            ScaleUnderZero     = status & 0x4     # bit 2 : Under Zero / In Range
            ScaleOutOfRange    = status & 0x8     # bit 3 : Outside Zero Capture Range / In Range
            ScaleCenterOfZero  = status & 0x10    # bit 4 : Center of Zero / Not at center of zero
            ScaleNetWeight     = status & 0x20    # bit 5 : Net Weight / Gross Weight
            ScaleChangeOccured = status & 0x40    # bit 6 : Normal or Weight Change/ Bad Host Command
            ScaleBadCommand    = not ScaleChangeOccured
            '''
            if ScaleInMotion != 0:
                print("Scale In Motion")
            if ScaleOutOfRange != 0:
                print("Out of Range")
            if ScaleBadCommand == 1:
                print("Bad Command")
            '''
        else:
            # Decode the Weight and convert it into grams
            Weightlb = float(ArivaString.decode())
            Weightgr = Weightlb * 435.592
            Weighttare = Weightlb - tare
            if Weighttare <= 0:
                Weighttare = 0
            #print(Weightgr)
            msg = ProduceScaleMsg()
            msg.header.stamp = rospy.Time.now()
            msg.header.seq = self.counter
            #msg.weight = Weightlb
            msg.weight = Weighttare
            self.pubs['weight'].publish(msg)
            #return(Weightgr)


if __name__ == "__main__":
    produce_scale = ProduceScale()



